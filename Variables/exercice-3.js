const String = "42";
const Number = 42;
const Float = 42.00;
const Boolean = true;
const Array = [42];
const Date42 = new Date(42);
const Object = {
	valeur : '42'
};
const Undefined = undefined;
const Null = null;

console.log(String);
console.log(Number);
console.log(Float);
console.log(Boolean);
console.log(Array);
console.log(Date42);
console.log(Object.valeur);
console.log(Undefined);
console.log(Null);
