const my42String = "42";
const my42Number = 42;
const my42Float = 42.00;
const my42Boolean = true;
const my42Array = [42];
const my42Date42 = new Date(42);
const my42Object = {
	valeur : '42'
};
const my42Undefined = undefined;
const my42Null = null;

console.log(typeof my42String);
console.log(typeof my42Number);
console.log(typeof my42Float);
console.log(typeof my42Boolean);
console.log(typeof my42Array);
console.log(typeof my42Date42);
console.log(typeof my42Object);
console.log(typeof my42Undefined);
console.log(typeof my42Null);